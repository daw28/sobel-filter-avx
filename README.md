# AXV Sobel Filter

Written to Satisfy Assignment four of COMP311-17B at Waikato.

A Sobel filter implemented in C using AVX instructions.
Check out Sobel.c for implementation. Should compile for any system supporting AVX and gcc